# README #

### What does this application do? ###
This application allow to calculate the score of a bowling game played by n persons, and show a table with each frame played and the accumulated score

### Prerequisites ###
To download and run correctly the project, you must have to have installed:

    * JDK 8
    * Apache Maven 3.6.3

### How deploy the code? ###
    1. Clone this project in the folder where you want to work.
    2. Go to the folder and execute "mvn install" to download all the dependencies needed.
    3. Open the project with your ide to start working on the code.

### How to create the executable jar file? ###
    1. Go to the root of the project and execute "mvn package".
    2. Go to target folder and there you have your .jar file. (By default is "score-bowling-1.0-SNAPSHOT.jar")

### How do I use the application? ###
Once you have the executable .jar, you must run it in a terminal with _"java -jar NameApp.jar"_. The application will prompt you that input the path of the file with the data to process and show you a table with the score of
the players.
An alternative way is to use _-p_ argument when execute the application, remaining as follows _"java -jar NameApp.jar -p'PathOfFile'"_ where PathOfFile is the path of the data to process. When you use this argument, the 
application does not ask you for a path, and will generate the table with the scores.
You can get an example of the data file inside the folder "dataExample" in this repository with the name TestData.txt.

### File data format ###
The file is expected to have two columns: The first indicate the name of the player that made the throw and the second is the number of pins knocked down.

The second column only allow values between 0 and 10, and a F when the player commits a fault.

Example of a data file:

[![Example of file data.](https://iili.io/2c6ECv.md.png)](https://freeimage.host/i/2c6ECv)