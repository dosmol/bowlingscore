package com.challenge.bowling.IntegrationTest;

import com.challenge.bowling.model.list.FrameList;
import com.challenge.bowling.model.list.LineList;
import com.challenge.bowling.service.BowlingService;
import com.challenge.bowling.service.InputOutputService;
import java.io.File;
import java.util.HashMap;
import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;
import org.junit.Test;

/**
 *
 * @author Martín Cabo
 */
public class IntegrationTest {

    String testPath = "";
    String perfectPath = "";
    String worsePath = "";
    InputOutputService ioService;
    BowlingService bowlingService;

    public IntegrationTest() {
        this.testPath = System.getProperty("user.dir") + File.separator + "dataExample" + File.separator + "TestData.txt";
        this.perfectPath = System.getProperty("user.dir") + File.separator + "dataExample" + File.separator + "PerfectScore.txt";
        this.worsePath = System.getProperty("user.dir") + File.separator + "dataExample" + File.separator + "WorseScore.txt";
        this.ioService = new InputOutputService();
        this.bowlingService = new BowlingService();

    }

    @Test
    public void twoPlayersTestTest() {
        HashMap<String, FrameList> game = this.ioService.fileMapper("BadPath");
        assertTrue(null == game);

        game = this.ioService.fileMapper(this.testPath);
        assertFalse(null == game);
        game.forEach((player, frames) -> {
            assertTrue(frames.getLast().getPinsFalls().size() > 1 && frames.getLast().getPinsFalls().size() <= 3);
            assertTrue(frames.size() == 10);
        });

        LineList lines = bowlingService.calculateScore(game);
        lines.forEach(line -> {
            assertTrue(null != line.getPlayer());
            assertFalse(line.getTotalScore()< 0 && line.getTotalScore()> 300);
        });
    }
    
    @Test
    public void perfectGameTest() {
        HashMap<String, FrameList> game = this.ioService.fileMapper("BadPath");
        assertTrue(null == game);

        game = this.ioService.fileMapper(this.perfectPath);
        assertFalse(null == game);
        game.forEach((player, frames) -> {
            assertTrue(frames.getLast().getPinsFalls().size() > 1 && frames.getLast().getPinsFalls().size() <= 3);
            assertTrue(frames.size() == 10);
        });

        LineList lines = bowlingService.calculateScore(game);
        lines.forEach(line -> {
            if("Jeff".equals(line.getPlayer())){
                assertTrue(line.getTotalScore()==300);
            }else{
                assertFalse(line.getTotalScore()< 0 && line.getTotalScore()> 300);
            }

        });
    }
    
    @Test
    public void worseGameTest() {
        HashMap<String, FrameList> game = this.ioService.fileMapper("BadPath");
        assertTrue(null == game);

        game = this.ioService.fileMapper(this.worsePath);
        assertFalse(null == game);
        game.forEach((player, frames) -> {
            assertTrue(frames.getLast().getPinsFalls().size() > 1 && frames.getLast().getPinsFalls().size() <= 3);
            assertTrue(frames.size() == 10);
        });

        LineList lines = bowlingService.calculateScore(game);
        lines.forEach(line -> {
            if("Jeff".equals(line.getPlayer())){
                assertTrue(line.getTotalScore()==0);
            }else{
                assertFalse(line.getTotalScore()< 0 && line.getTotalScore()> 300);
            }

        });
    }

}
