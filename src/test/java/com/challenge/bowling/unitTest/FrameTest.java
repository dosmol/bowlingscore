package com.challenge.bowling.unitTest;

import com.challenge.bowling.model.Frame;
import java.util.ArrayList;
import static junit.framework.TestCase.assertTrue;
import static junit.framework.TestCase.assertEquals;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Martín Cabo
 */
public class FrameTest {

    ArrayList<String> values = new ArrayList<>();

    @Before
    public void loadData() {
        this.values.add("2");
        this.values.add("3");
        this.values.add("2");
        this.values.add("F");
        this.values.add("6");
    }

    @Test
    public void getDroppedPinesTest() {
        Frame frame = new Frame();
        frame.setPinsFalls(this.values);

        //Test the method without parameter
        assertTrue(frame.getDroppedPines() == 13);

        //Test the method with paramater
        assertTrue(frame.getDroppedPines(2) == 5);
        assertTrue(frame.getDroppedPines(4) == 7);

    }

    @Test
    public void getPinsFallsAt() {
        Frame frame = new Frame();
        frame.setPinsFalls(this.values);

        assertEquals(true, frame.getPinsFallsAt(3) == 0);
        assertEquals(false, frame.getPinsFallsAt(3) == 6);
        assertEquals(true, frame.getPinsFallsAt(13) == 0);
    }

}
