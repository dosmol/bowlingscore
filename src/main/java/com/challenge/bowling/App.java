package com.challenge.bowling;

import com.challenge.bowling.controller.ProcessController;
import java.util.Scanner;

public class App {

    private static String filePath = "";
    private static final String PREFIX = "-p";

    public static void main(String[] args) {
        if (0 < args.length) {
            for (String arg : args) {//Verify that if the path has been sent in the arguments
                if (arg.toLowerCase().startsWith(PREFIX.toLowerCase())) {
                    filePath = arg.substring(PREFIX.length(), arg.length());
                }
            }
        } else {//IIf the path is not in the argument, ask to the user for it.
            Scanner scanner = new Scanner(System.in);
            System.out.print("Input a file path: ");
            filePath = scanner.nextLine();
            System.out.flush();
        }

        if (!filePath.isEmpty()) {//If no path has been introduced, show the situation and finish the program.
            ProcessController pc = new ProcessController();
            pc.processFile(filePath);
        } else {
            System.out.println("Error. You must introduce a file path.");
        }

    }
}
