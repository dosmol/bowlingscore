package com.challenge.bowling.controller;

import com.challenge.bowling.model.list.FrameList;
import com.challenge.bowling.model.list.LineList;
import com.challenge.bowling.service.BowlingService;
import com.challenge.bowling.service.InputOutputService;
import java.util.HashMap;

/**
 *
 * @author Martín Cabo
 */
public class ProcessController {

    BowlingService service;
    InputOutputService ioService;

    public ProcessController() {
        //create the instances of the services
        this.service = new BowlingService();
        this.ioService = new InputOutputService();
    }

    /**
     *
     * This method receives a path from the file, and call the services to
     * process it and show the output in the screen.
     *
     * @param filePath A String indicating path of file with the data
     * @return Void.
     * @author Martín Cabo
     */
    public void processFile(String filePath) {
        try {
            HashMap<String, FrameList> game = ioService.fileMapper(filePath);
            if (game != null) {
                LineList lines = service.calculateScore(game);
                ioService.drawOutput(lines);
            }

        } catch (Exception e) {
            System.err.println("We are having some problems precessing the file");//General problem is notificated to the user.
        }
    }
}
