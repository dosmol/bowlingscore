package com.challenge.bowling.model.list;

import com.challenge.bowling.model.Frame;
import java.util.ArrayList;

/**
 *
 * @author Martín Cabo
 */
public class FrameList extends ArrayList<Frame> {

    public Frame getLast() {
        return this.get(this.size() - 1);
    }
}
