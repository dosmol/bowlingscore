package com.challenge.bowling.model;

import java.util.ArrayList;

/**
 *
 * @author Martín Cabo
 */
public class Frame {

    public static final Integer MAX_PINES = 10;
    public static final Integer SRTIKE_BONUS = 10;
    public static final Integer SPARE_BONUS = 10;
    public static final String FAULT_VALUE = "F";

    private ArrayList<String> pinsFalls = new ArrayList<String>();
    private Integer score;

    public Frame() {
    }

    public Frame(ArrayList<String> pinsFalls, Integer score) {
        this.pinsFalls = pinsFalls;
        this.score = score;
    }

    public ArrayList<String> getPinsFalls() {
        return pinsFalls;
    }

    public void setPinsFalls(ArrayList<String> pinsFalls) {
        this.pinsFalls = pinsFalls;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public Integer getDroppedPines() {
        Integer total = 0;
        for (String p : this.pinsFalls) {
            total += FAULT_VALUE.equals(p) ? 0 : Integer.parseInt(p);
        }
        return total;
    }

    public Integer getDroppedPines(Integer limitIndex) {
        Integer total = 0;

        if (limitIndex > this.pinsFalls.size()) {
            return 0;
        }

        for (int i = 0; i < limitIndex; i++) {
            total += FAULT_VALUE.equals(this.pinsFalls.get(i)) ? 0 : Integer.parseInt(this.pinsFalls.get(i));
        }
        return total;
    }

    public Integer getPinsFallsAt(Integer index) {
        if (index > this.pinsFalls.size())return 0;
        return FAULT_VALUE.equals(this.pinsFalls.get(index)) ? 0 : Integer.parseInt(this.pinsFalls.get(index));
    }
}
