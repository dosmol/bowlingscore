package com.challenge.bowling.model;

import com.challenge.bowling.model.list.FrameList;

/**
 *
 * @author Martín Cabo
 */
public class Line {

    public static Integer MAX_FRAMES = 10;

    private String player;
    private FrameList frames = new FrameList();
    private Integer totalScore = 0;

    public Line() {
    }

    public Line(String player, FrameList frames, Integer totalScore) {
        this.player = player;
        this.frames = frames;
    }

    public Line(String player) {
        this.player = player;
    }

    public String getPlayer() {
        return player;
    }

    public void setPlayer(String player) {
        this.player = player;
    }

    public FrameList getFrames() {
        return frames;
    }

    public void setFrames(FrameList frames) {
        this.frames = frames;
    }

    public Integer getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(Integer totalScore) {
        this.totalScore = totalScore;
    }

    public void addPoints(Integer points) {
        this.totalScore += points;
    }
}
