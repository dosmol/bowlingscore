package com.challenge.bowling.service;

import com.challenge.bowling.model.Frame;
import com.challenge.bowling.model.Line;
import com.challenge.bowling.model.list.FrameList;
import com.challenge.bowling.model.list.LineList;
import com.challenge.bowling.service.interfaces.GamesInterface;
import java.util.HashMap;

/**
 *
 * @author Martín Cabo
 */
public class BowlingService implements GamesInterface {

    /**
     *
     * This method calculates the score of the players along all their throws.
     *
     * @param game A HasMap with the player and all his played frames
     * @return LineList with the score for each player
     * @author Martín Cabo
     */
    @Override
    public LineList calculateScore(HashMap<String, FrameList> game) {
        LineList lines = new LineList();
        game.forEach((player, frames) -> { //Iterate all the elements of my hashMap.
            Line actualLine = new Line(player); //Create a new Line from my model, and set the player.
            for (int in = 0; in < frames.size(); in++) { //Iterate each frame from my FrameList.
                Frame frame = frames.get(in);
                Integer pinesDropped = frame.getDroppedPines(); //Get how many lines has been down in this frame.
                if (frame.getDroppedPines() < Frame.MAX_PINES) {//Validate if the player did not make a Strike or a Spare.
                    actualLine.addPoints(pinesDropped);
                } else if (frame.getPinsFalls().size() == 1) { //The player made Strike
                    if (in == frames.size() - 2) {//Ask if I am at the pre last position
                        actualLine.addPoints(frames.get(in + 1).getDroppedPines(2) + Frame.SRTIKE_BONUS);//Add the totals point of the first two plays of the last frame to the score whith the Strike bonus.
                    } else if (in == frames.size() - 1) {//Ask if I am at the last position
                        actualLine.addPoints(frames.get(in).getDroppedPines());//Get how many lines has been down in this frame.
                    } else {//I am not in the pre or last position.
                        if (frames.get(in + 1).getPinsFalls().size() > 1) {
                            actualLine.addPoints(frames.get(in + 1).getDroppedPines() + Frame.SRTIKE_BONUS);//If the next frame has more than 1 throw, I use them to my total + the strike bonus.
                        } else {
                            //If the next frame didn't has more than 1 throw, I use all the next and the first of the next of the next to my total + the strike bonus.
                            actualLine.addPoints(frames.get(in + 1).getDroppedPines() + frames.get(in + 2).getPinsFallsAt(0) + Frame.SRTIKE_BONUS);
                        }
                    }
                } else {//Made spare
                    if (in == frames.size() - 2) {//Ask if I am at the pre last position
                        actualLine.addPoints(frames.get(in + 1).getDroppedPines(1) + Frame.SPARE_BONUS);//Get the totals point of the first play of the last frame + Spare Bonus.
                    } else if (in == frames.size() - 1) {//Ask if I am at the last position
                        actualLine.addPoints(frames.get(in).getDroppedPines());//Get how many lines has been down in this frame.
                    } else {
                        //Use the first of the next  to my total + the Spare bonus.
                        actualLine.addPoints(frames.get(in + 1).getPinsFallsAt(0) + Frame.SPARE_BONUS);
                    }
                }
                frame.setScore(actualLine.getTotalScore());//Set the total to the frame
                actualLine.getFrames().add(frame);
            }
            lines.add(actualLine);//Add my actual  line at the LinesList.
        });

        return lines;
    }

}
