/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.challenge.bowling.service.interfaces;

import com.challenge.bowling.model.list.FrameList;
import com.challenge.bowling.model.list.LineList;
import java.util.HashMap;

/**
 *
 * @author Martín Cabo
 */
public interface GamesInterface {

    public LineList calculateScore(HashMap<String, FrameList> game);

}
