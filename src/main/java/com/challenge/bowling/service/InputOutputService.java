/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.challenge.bowling.service;

import com.challenge.bowling.model.Frame;
import com.challenge.bowling.model.Line;
import com.challenge.bowling.model.list.FrameList;
import com.challenge.bowling.model.list.LineList;
import com.challenge.bowling.service.interfaces.InputOutpuInterface;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

/**
 *
 * @author Martín Cabo
 */
public class InputOutputService implements InputOutpuInterface {

    /**
     *
     * This method allow to map the file input into a java classes.
     *
     * @param filePath as the path where the data is storage.
     * @return A HashMap with the mapped data, or null if there is any error.
     * @author Martín Cabo
     */
    @Override
    public HashMap<String, FrameList> fileMapper(String filePath) {
        String line;
        String actualPlayer = "";
        HashMap<String, FrameList> result = new HashMap<>();

        try {
            //Read the file, make data validation and fill the hashMap.
            BufferedReader br = new BufferedReader(new FileReader(filePath));
            while ((line = br.readLine()) != null) {//iterate
                Frame frame;
                String[] row = line.split("\\s");

                if (row.length == 2) {
                    if (Frame.FAULT_VALUE.equals(row[1]) || (Integer.parseInt(row[1].trim()) >= 0 && Integer.parseInt(row[1].trim()) <= 10)) {

                        if (actualPlayer.equals(row[0].trim())) {
                            frame = result.get(row[0].trim()).getLast();
                            frame.getPinsFalls().add(row[1].trim());
                        } else {
                            frame = new Frame();
                            frame.getPinsFalls().add(row[1].trim());
                            FrameList frames;
                            if (result.containsKey(row[0].trim())) {
                                frames = result.get(row[0].trim());
                                frames.add(frame);
                            } else {
                                frames = new FrameList();
                                frames.add(frame);
                                result.put(row[0].trim(), frames);
                            }
                        }
                        if (result.get(row[0]).size() < 10) {
                            if (result.get(row[0]).getLast().getPinsFalls().size() > 2) {
                                System.err.println("The data in the file is not correct. You can not have more than 2 throws in the frame.");
                            }
                        } else {
                            if (result.get(row[0]).getLast().getPinsFalls().size() > 3) {
                                System.err.println("The data in the file is not correct. You can not have more than 3 throws in the frame.");
                            }
                        }

                        actualPlayer = row[0].trim();
                    } else {
                        System.err.println("The data in the file is not correct. You only can use values between 0 and 10, or F.");
                    }
                } else {
                    System.err.println("The file can have only 2 columns.");
                }

            }
            result.forEach((player, frames) -> {
                if (frames.size() != 10) {
                    System.err.println("The data in the file is not correct. The player " + player + " has " + frames.size() + " throws and only 10 are accepted.");
                }
            });
            return result;
        } catch (FileNotFoundException fnfe) {
            System.err.println("We can' find the file in the path " + filePath);
        } catch (NumberFormatException nfe) {
            System.err.println("The data in the file is not correct. You only can use values between 0 and 10, or F.");
        } catch (IOException ioe) {
            System.err.println("There was an error reading the file.");
        }
        return null;
    }

    /**
     *
     * This method draw the output table with the score of the players.
     *
     * @param lines ArrayList of Lines which contains all the frames and scores.
     * @return Void.
     * @author Martín Cabo
     */
    @Override
    public void drawOutput(LineList lines) {
        System.out.print("Frame\t\t");
        for (int i = 1; i <= Line.MAX_FRAMES; i++) {
            System.out.print(i + "\t\t"); //Print the number of frames.
        }
        System.out.println("");
        lines.forEach(line -> { //Iterate each line
            System.out.println(line.getPlayer());
            System.out.print("Pinfalls\t");
            line.getFrames().forEach(frame -> {
                if (frame.getDroppedPines(2) == Frame.MAX_PINES && frame.getPinsFalls().size() > 1) {
                    frame.getPinsFalls().set(1, "/");//If is a second throw and have max pine vale, set a / simbol on that position.
                }
                frame.getPinsFalls().forEach(pf -> {
                    if (Frame.MAX_PINES.toString().equals(pf) && frame.getPinsFalls().size() != 3) {
                        System.out.print("\t");//If I have de max pines and don't have 3 throws, add a tab
                    }
                    System.out.print(Frame.MAX_PINES.toString().equals(pf) ? "X\t" : pf + "\t");//If the value is max pines, write X simbol, else the value.
                });
            });
            System.out.println("");
            System.out.print("Score\t\t");
            line.getFrames().forEach(frame -> {
                System.out.print(frame.getScore() + "\t\t");//Write the scores for each frame.
            });
            System.out.println("");
        });

    }
}
